package edu.ntnu.idatt2001.alekhal.patientregister;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class made to keep track of and manage Patient objects
 */
public class PatientRegister {

    private ArrayList<Patient> patients;
    private File saveFile;
    private final PatientDAO DAO;

    public PatientRegister() {
        patients = new ArrayList<>();
        DAO = new PatientDAO();
    }

    public boolean addPatient(String socialSecurityNumber, String firstName, String lastName) {
        for (Patient patient : patients) {
            if (patient.getSocialSecurityNumber().equals(socialSecurityNumber)) {
                throw new IllegalArgumentException("Duplicate socialSecurityNumber");
            }
        }
        if (patients.add(new Patient(firstName, lastName, "", socialSecurityNumber))) {
            return true;
        }
        return false;
    }

    public boolean addPatient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis) {
        for (Patient patient : patients) {
            if (patient.getSocialSecurityNumber().equals(socialSecurityNumber)) {
                throw new IllegalArgumentException("Duplicate socialSecurityNumber");
            }
        }
        if (patients.add(new Patient(firstName, lastName, generalPractitioner, socialSecurityNumber, diagnosis))) {
            return true;
        }
        return false;
    }

    public boolean removePatient(String socialSecurityNumber) {
        for (Patient patient : patients) {
            if (patient.getSocialSecurityNumber() == socialSecurityNumber) {
                patients.remove(patient);
                return true;
            }
        }
        return false;
    }

    public Patient getPatient(String socialSecurityNumber) {
        for (Patient patient : patients) {
            if (patient.getSocialSecurityNumber().equals(socialSecurityNumber)) {
                return patient;
            }
        }
        return null;
    }

    public ObservableList<Patient> getPatients() {
        ObservableList<Patient> tempPatients = FXCollections.observableArrayList();
        for (Patient patient : patients) {
            tempPatients.add(patient);
        }
        return tempPatients;
    }

    public void clearRegister() {
        patients = new ArrayList<>();
    }

    public File getSaveFile() {
        return saveFile;
    }

    public void setSaveFile(File saveFile) {
        this.saveFile = saveFile;
    }

    /**
     * Loads patients from a csv file
     */
    public void load() {
        DAO.fillRegisterFromCSV(this);
    }

    /**
     * Saves patients to a csv file
     */
    public void save() {
        DAO.saveToSCV(this);
    }
}