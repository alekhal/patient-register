package edu.ntnu.idatt2001.alekhal.patientregister.controllers;

import edu.ntnu.idatt2001.alekhal.patientregister.App;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Window;

public class DeleteConfirmationController {

    @FXML
    Button cancelBtn;
    @FXML
    Button okBtn;

    /**
     * Removes the selected patient from the patientRegister
     */
    @FXML
    public void delete() {
        App.getPatientRegister().removePatient(PrimaryController.selectedSocialSecurityNumber);
        Window window = cancelBtn.getScene().getWindow();
        window.hide();
    }

    /**
     * Closes the window
     */
    @FXML
    public void close() {
        Window window = cancelBtn.getScene().getWindow();
        window.hide();
    }

}