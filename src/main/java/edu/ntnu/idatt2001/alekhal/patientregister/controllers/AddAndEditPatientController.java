package edu.ntnu.idatt2001.alekhal.patientregister.controllers;

import edu.ntnu.idatt2001.alekhal.patientregister.App;
import edu.ntnu.idatt2001.alekhal.patientregister.PatientRegister;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;

public class AddAndEditPatientController {

    @FXML
    TextField firstName;
    @FXML
    TextField lastName;
    @FXML
    TextField socialSecurityNumber;

    @FXML
    Button cancelBtn;
    @FXML
    Button okBtn;

    PatientRegister register;

    /**
     * If setEdit is true it populates the text fields with the information from the currently selected task
     */
    public void initialize() {
        if (PrimaryController.setEdit == true) {
            firstName.setText(App.getPatientRegister().getPatient(PrimaryController.selectedSocialSecurityNumber).getFirstName());
            lastName.setText(App.getPatientRegister().getPatient(PrimaryController.selectedSocialSecurityNumber).getLastName());
            socialSecurityNumber.setText(PrimaryController.selectedSocialSecurityNumber);
        }
    }

    /**
     * If setEdit is true it deletes the selected patient and adds the edited one.
     * If setEdit is false it adds the new patient
     *
     * @param event
     */
    @FXML
    public void addPatient(ActionEvent event) {
        try {
            if (socialSecurityNumber.getText().matches("^[0-9]*$")) {
                if (PrimaryController.setEdit == true) {
                    App.getPatientRegister().removePatient(PrimaryController.selectedSocialSecurityNumber);
                    App.getPatientRegister().addPatient(socialSecurityNumber.getText(), firstName.getText(), lastName.getText());
                    close();
                } else {
                    App.getPatientRegister().addPatient(socialSecurityNumber.getText(), firstName.getText(), lastName.getText());
                    close();
                }
            } else {
                invalidSocialSecurity();
            }
        } catch (IllegalArgumentException e) {
            invalidSocialSecurity();
        }
    }

    /**
     * Closes the window
     */
    @FXML
    public void close() {
        Window window = cancelBtn.getScene().getWindow();
        window.hide();
    }

    public void invalidSocialSecurity() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/invalidSocialSecurityNumber.fxml").toURI().toURL());
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setMaxHeight(190);
            stage.setMaxWidth(375);
            stage.setMinHeight(190);
            stage.setMinWidth(375);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}