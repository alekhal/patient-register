package edu.ntnu.idatt2001.alekhal.patientregister;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class PatientDAO {
    public final String DELIMITER = ";";

    public PatientDAO() {
    }

    /**
     * Uses FileInputStream to read patients from CSV file and adds them to the register
     * Uses try with resource to automatically close the connection if it fails
     * Prints out the stack trace if IOException is thrown
     *
     * @param patientRegister register to add patients to
     */
    public void fillRegisterFromCSV(PatientRegister patientRegister) {
        String line;
        try (FileInputStream fileInputStream = new FileInputStream(patientRegister.getSaveFile());
             InputStreamReader isr = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {
            while ((line = br.readLine()) != null) {
                if (!line.isEmpty()) {
                    String[] patient = line.split(DELIMITER);
                    patientRegister.addPatient(patient[3], patient[0], patient[1]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uses OutputStream and convertToCSV to save patients to CSV file
     * Uses try with resource to automatically close the connection if it fails
     * Prints out the stack trace if IOException is thrown
     *
     * @param patientRegister register to be saved
     */
    public void saveToSCV(PatientRegister patientRegister) {
        try (OutputStream os = new FileOutputStream(patientRegister.getSaveFile());
             PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
            patientRegister.getPatients().stream().map(Patient::convertToCSV).forEach(pw::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
