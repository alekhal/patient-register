package edu.ntnu.idatt2001.alekhal.patientregister;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

/**
 * This class starts the application
 *
 * @author Aleksander H Holthe
 */

public class App extends Application {
    private static Scene scene;
    private static PatientRegister patientRegister;

    /**
     * Sets the root of the scene
     *
     * @param fxml the name of the fxml file to set as root
     * @throws IOException if the fxml file is not found
     */
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Loads in a fxml file from a string
     *
     * @param fxml t he name of the fxml file to find and return
     * @return the fxml file loaded
     * @throws IOException if the file is not found
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/" + fxml + ".fxml").toURI().toURL());
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

    public static PatientRegister getPatientRegister() {
        return patientRegister;
    }

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("Primary"), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
        patientRegister = new PatientRegister();
    }
}