package edu.ntnu.idatt2001.alekhal.patientregister.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Window;

public class InvalidFileTypeController {
    @FXML
    Button cancelBtn;

    /**
     * Closes the window
     */
    @FXML
    public void close() {
        Window window = cancelBtn.getScene().getWindow();
        window.hide();
    }

    /**
     * Cancels file selection by breaking the loop in loadCSV in PrimaryController
     */
    public void cancel() {
        PrimaryController.isDone = true;
        close();
    }

    public void newFile() {
        close();
    }
}
