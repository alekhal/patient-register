package edu.ntnu.idatt2001.alekhal.patientregister.controllers;

import edu.ntnu.idatt2001.alekhal.patientregister.App;
import edu.ntnu.idatt2001.alekhal.patientregister.Patient;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;


public class PrimaryController {

    @FXML
    TableView<Patient> table;
    @FXML
    TableColumn<Patient, String> firstName;
    @FXML
    TableColumn<Patient, String> lastName;
    @FXML
    TableColumn<Patient, Integer> socialSecurityNumber;

    static String selectedSocialSecurityNumber;
    static boolean setEdit;
    static boolean isDone;


    /**
     * Opens and sets properties for the applicationInfo window
     */
    @FXML
    public void applicationInfo() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/programInfo.fxml").toURI().toURL());
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setMaxHeight(190);
            stage.setMaxWidth(375);
            stage.setMinHeight(190);
            stage.setMinWidth(375);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens and sets properties for the selectPatientError window
     */
    @FXML
    public void selectPatientError() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/selectPatientError.fxml").toURI().toURL());
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setMaxHeight(190);
            stage.setMaxWidth(375);
            stage.setMinHeight(190);
            stage.setMinWidth(375);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens and sets properties for the InvalidFileType window
     */
    @FXML
    public void invalidFileType() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/invalidFileType.fxml").toURI().toURL());
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setMaxHeight(190);
            stage.setMaxWidth(375);
            stage.setMinHeight(190);
            stage.setMinWidth(375);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets setEdit to false and opens the AddAndEditPatient window
     */
    @FXML
    public void addPatient() {
        setEdit = false;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/addAndEditPatient.fxml").toURI().toURL());
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
            update();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * If the currently selected patient is valid it opens the AddAndEditPatient window and sets setEdit to true
     * otherwise it runs the selectPatientError method
     */
    @FXML
    public void editPatient() {
        setEdit = false;
        if (table.getSelectionModel().getSelectedItem() != null) {
            try {
                setEdit = true;
                selectedSocialSecurityNumber = table.getSelectionModel().getSelectedItem().getSocialSecurityNumber();

                FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/addAndEditPatient.fxml").toURI().toURL());
                Parent root = fxmlLoader.load();

                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
            } catch (IOException e) {
                e.printStackTrace();
            }
            update();
        } else {
            selectPatientError();
        }
    }

    /**
     * Opens and sets properties for the deletePatient window as long as the selected patient is valid.
     */
    @FXML
    public void deletePatient() {
        if (table.getSelectionModel().getSelectedItem() != null) {
            try {
                selectedSocialSecurityNumber = table.getSelectionModel().getSelectedItem().getSocialSecurityNumber();

                FXMLLoader fxmlLoader = new FXMLLoader(new File("src/main/resources/fxml/deleteConfirmation.fxml").toURI().toURL());
                Parent root = fxmlLoader.load();

                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setMaxHeight(190);
                stage.setMaxWidth(375);
                stage.setMinHeight(190);
                stage.setMinWidth(375);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();

                update();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            selectPatientError();
        }
    }

    /**
     * Runs a loop that allows you to select a file to load patients from.
     * If you select an invalid file you get the choice to select a new one.
     * The loop is broken if you select a valid file or press cancel.
     */
    public void loadCSV() {
        isDone = false;
        while (!isDone) {
            App.getPatientRegister().clearRegister();
            FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            File file = fileChooser.showOpenDialog(stage);
            if (file != null && getFileType(file).equals("csv")) {
                App.getPatientRegister().setSaveFile(file);
                App.getPatientRegister().load();
                update();
                isDone = true;
            } else {
                if (file != null) {
                    invalidFileType();
                } else {
                    isDone = true;
                }
            }
        }
    }

    /**
     * Opens a window that allows you to save the TaskRegister as a CSV file.
     */
    public void saveCSV() {
        try{
            FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            File file = fileChooser.showSaveDialog(stage);
            App.getPatientRegister().setSaveFile(file);
            App.getPatientRegister().save();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the file type of any file that is sent in.
     *
     * @param file File to check filetype for
     * @return String containing file type
     */
    public String getFileType(File file) {
        String fileName = file.getName();
        int dotIndex = fileName.lastIndexOf('.');
        return fileName.substring(dotIndex + 1);
    }

    public void exitApplication(){
        table.getScene().getWindow().hide();
    }

    /**
     * Refreshes the table view allowing all current patients to show up
     */
    public void update() {
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        table.setItems(App.getPatientRegister().getPatients());
    }
}
