package edu.ntnu.idatt2001.alekhal.patientregister.factory;

import edu.ntnu.idatt2001.alekhal.patientregister.Patient;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

/**
 * Factory for creating nodes and alerts
 */
public class GUIFactory {

    /**
     * Creates nodes that can be used some places in the code.
     *
     * @param type Type of node that is to be created
     * @return the requested node.
     */
    public static Node getNode(String type) {
        if (type.equalsIgnoreCase("Button")) {
            return new Button();
        } else if (type.equalsIgnoreCase("Textfield")) {
            return new TextField();
        } else if (type.equalsIgnoreCase("Label")) {
            return new Label();
        } else if (type.equalsIgnoreCase("Gridpane")) {
            return new GridPane();
        } else if (type.equalsIgnoreCase("TableView")) {
            return new TableView<>();
        } else if (type.equalsIgnoreCase("ImageView")) {
            return new ImageView();
        } else if (type.equalsIgnoreCase("MenuBar")) {
            return new MenuBar();
        }
        return null;
    }

    /**
     * Creates an alert that can be used to show information about a chosen patient
     *
     * @param patient The chosen patient
     */
    public static void patientInfo(Patient patient) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText("Information about " + patient.getFirstName() + " " + patient.getLastName());
        alert.setContentText("Name: " + patient.getFirstName() + " " + patient.getLastName() + "\n" + "Social security number: " +
                patient.getSocialSecurityNumber() + "\n" + "General Practitioner: " +
                patient.getGeneralPractitioner() + "\n" + "Diagnosis: " + patient.getDiagnosis());
        alert.showAndWait();
    }

    /**
     * Creates an alert box that can be used to alert the user about errors
     *
     * @param titleMessage Title of the alert box
     * @param reason       Reason for error
     */
    public static void displayErrorMessage(String titleMessage, String reason) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(titleMessage);
        alert.setContentText(reason);
        alert.showAndWait();
    }
}