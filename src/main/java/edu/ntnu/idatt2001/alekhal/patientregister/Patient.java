package edu.ntnu.idatt2001.alekhal.patientregister;

/**
 * Class representing a patient.
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.generalPractitioner = generalPractitioner;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Creates and returns a String in the correct format for the csv file.
     *
     * @return a string for the csv file
     */
    public String convertToCSV() {
        return firstName + ";" + lastName + ";" + "" + ";" + socialSecurityNumber;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber=" + socialSecurityNumber +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}