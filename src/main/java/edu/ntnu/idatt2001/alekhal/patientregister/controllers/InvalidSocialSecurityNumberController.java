package edu.ntnu.idatt2001.alekhal.patientregister.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Window;

public class InvalidSocialSecurityNumberController {
    @FXML
    Button closeBtn;

    /**
     * Closes the window
     */
    @FXML
    public void close() {
        Window window = closeBtn.getScene().getWindow();
        window.hide();
    }
}
