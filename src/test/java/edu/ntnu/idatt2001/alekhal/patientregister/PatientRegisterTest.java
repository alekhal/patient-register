package edu.ntnu.idatt2001.alekhal.patientregister;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {
    private PatientRegister register;

    @BeforeEach
    public void setUp() {
        this.register = new PatientRegister();
        register.addPatient("21321", "Aleks", "Holthe");
    }

    @Test
    void addNewPatient() {
        Assertions.assertTrue(register.addPatient("21321321", "test", "testeson"));
    }

    @Test
    void addDuplicatePatient() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    register.addPatient("21321", "Aleks", "Holthe");
                });
    }

    @Test
    void removeExistingPatient() {
        Assertions.assertTrue(register.removePatient("21321"));
    }

    @Test
    void removeNonExsistingPatient() {
        Assertions.assertFalse(register.removePatient("1111111"));
    }
}