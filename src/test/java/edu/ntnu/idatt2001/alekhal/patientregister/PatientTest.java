package edu.ntnu.idatt2001.alekhal.patientregister;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {
    Patient patient;

    @BeforeEach
    void setUp() {
        patient = new Patient("Aleks", "Holthe", "", "211299");
    }

    @Test
    void convertToCSV() {
        Assertions.assertEquals("Aleks;Holthe;;211299", patient.convertToCSV());
    }
}