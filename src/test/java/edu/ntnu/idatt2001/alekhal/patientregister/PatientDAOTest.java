package edu.ntnu.idatt2001.alekhal.patientregister;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class PatientDAOTest {
    PatientRegister register;
    PatientRegister tempRegister;

    @BeforeEach
    void setUp() {
        tempRegister = new PatientRegister();
        tempRegister.addPatient("Andrø", "Tanker", "Maren G. Skake", "29104300764", "");
        tempRegister.addPatient("Ane", "Rikke", "Dr. Klø", "12073004096", "");
        register = new PatientRegister();
        register.setSaveFile(new File("src/test/resources/patients.csv"));
    }

    @Test
    void fillRegisterFromCSVTest() {
        register.load();
        Assertions.assertEquals(register.getPatient("12073004096").convertToCSV(), tempRegister.getPatient("12073004096").convertToCSV());
    }

    @Test
    void saveToSCVTest() {
        int nrOfLines = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("src/test/resources/patients.csv"))) {
            while (br.readLine() != null) {
                nrOfLines++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertEquals(2, nrOfLines);
    }
}